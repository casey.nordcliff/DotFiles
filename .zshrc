export EDITOR=vim
alias syyu='sudo pacman -Syyu'
alias c='gcc -Wall -std=c99 -O3'
alias purge='sudo pacman -Rsn $(pacman -Qdtq)'
alias startdocker='sudo systemctl start docker'
